using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[Flags]
public enum Modifiers
{
    Ctrl = 1,
    Alt = 2,
    Shift = 4,
    ShiftRight = 8
}

public class BuildingGeneratorTool : EditorWindow
{
    [MenuItem("Window/Tools/BuildingGeneratorTool")]
    public static void OpenBuildTool() => GetWindow<BuildingGeneratorTool>();
    public GameObject floor;
    [Range(1, 3)] public int floorCount;
    public GameObject window;
    public int windowsPerFloor;
    public GameObject roof;
    public GameObject door;
    public List<GameObject> prefabs;

    SerializedObject so;
    SerializedProperty _floor;
    SerializedProperty _window;
    SerializedProperty _roof;
    SerializedProperty _door;
    SerializedProperty _floorCount;
    public class SpawnPoint
    {
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 Up => rotation * Vector3.up;
        public bool isValid;
        public List<Dimensions> preafabMesurements = new List<Dimensions>();
        public SpawnPoint(Vector3 position, Quaternion rotation, List<GameObject> prefabs)
        {
            this.position = position;
            this.rotation = rotation;
            foreach (GameObject prefab in prefabs)
            {
                if (prefab != null)
                    this.preafabMesurements.Add(prefab.GetComponent<Dimensions>());
            }
        }
    }


    private void OnEnable()
    {
        so = new SerializedObject(this);
        _floorCount = so.FindProperty("floorCount");
        _window = so.FindProperty("window");
        _floor = so.FindProperty("floor");
        _door = so.FindProperty("door");
        _roof = so.FindProperty("roof");
        SceneView.duringSceneGui += DuringSceneGUI;
    }

    private void OnDisable()
    {
        SceneView.duringSceneGui -= DuringSceneGUI;
    }
    private void OnGUI()
    {
        so.Update();
        EditorGUILayout.PropertyField(_floor);
        EditorGUILayout.PropertyField(_floorCount);
        EditorGUILayout.PropertyField(_door);
        EditorGUILayout.PropertyField(_window);
        EditorGUILayout.PropertyField(_roof);

        if (so.ApplyModifiedProperties())
        {
            SceneView.RepaintAll();
        }

        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            GUI.FocusControl(null);
            Repaint();
        }
    }
    void DuringSceneGUI(SceneView sceneView)
    {
        Transform camTf = sceneView.camera.transform;

        if (Event.current.type == EventType.MouseMove)
        {
            sceneView.Repaint();
        }

        bool holdingAlt = (Event.current.modifiers & EventModifiers.Alt) != 0;
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit) && hit.collider != null)
        {
            if (hit.collider.CompareTag("Ground"))
            {
                Vector3 hitPosVisual = ExtensionMethods.Round(new Vector3(hit.point.x, hit.point.y + floor.transform.localScale.y / 2, hit.point.z));
                Matrix4x4 pointToWorldMtx = Matrix4x4.TRS(hitPosVisual, Quaternion.identity, Vector3.one);
                //Debug.Log(pointChosen.randAngle);
                //Debug.Log(pointChosen.spawnPrefab.name);

                MeshFilter[] filters = floor.GetComponentsInChildren<MeshFilter>();

                foreach (MeshFilter filter in filters)
                {
                    Matrix4x4 childToPoint = filter.transform.localToWorldMatrix;
                    Matrix4x4 childToWorldMtx = pointToWorldMtx * childToPoint;

                    Mesh mesh = filter.sharedMesh;
                    Material mat = filter.GetComponent<MeshRenderer>().sharedMaterial;
                    mat.SetPass(0);
                    //Graphics.DrawMeshNow(mesh, childToWorldMtx);
                    Graphics.DrawMesh(mesh, childToWorldMtx, mat, 0, sceneView.camera);
                }

                //Handles.DrawAAPolyLine(5, hit.point, hit.point + hit.normal);
                //buildingPlane = hit.collider.gameObject;
                if (Event.current.keyCode == KeyCode.G && Event.current.type == EventType.KeyDown && !holdingAlt)
                {
                    TrySpawnObject(hit);
                    //lastPlacedBuildingPlane = hit.collider.gameObject;
                    //Debug.Log(buildingPlane.name);
                }
            }
        }
    }
    GameObject selectedFolder;
    void TrySpawnObject(RaycastHit hit)
    {
        if (
            _floor == null ||
            _roof == null ||
            _window == null ||
            _door == null
            )
            return;
        prefabs.Clear();

        prefabs.Add(floor);
        prefabs.Add(window);
        prefabs.Add(roof);
        prefabs.Add(door);
        SpawnPoint Building = new SpawnPoint(hit.point, Quaternion.identity, prefabs);
        //selectedFolder = new GameObject(); //to remove
        //selectedFolder.transform.SetParent(buildingPlane.transform);

        selectedFolder = new GameObject("Building");
        selectedFolder.tag = "Building";
        selectedFolder.transform.position = ExtensionMethods.Round(new Vector3(Building.position.x, Building.position.y, Building.position.z));
        float lastFloorHeight = 0f;
        List<Dimensions> floors = new List<Dimensions>();
        foreach (Dimensions item in Building.preafabMesurements)
        {
            GameObject spawnedItem = (GameObject)PrefabUtility.InstantiatePrefab(item.gameObject);
            spawnedItem.transform.SetParent(selectedFolder.transform);
            Vector3 pos = ExtensionMethods.Round(new Vector3(Building.position.x, Building.position.y + item.height / 2, Building.position.z));
            spawnedItem.transform.position = pos;
            lastFloorHeight += item.height / 2;
            //DestroyImmediate(spawnedItem.GetComponent<Dimensions>());
            if (item is Floor)
            {
                floors.Add(item);
                for (int i = 1; i < floorCount; i++)
                {
                    floors.Add(item);
                    spawnedItem = (GameObject)PrefabUtility.InstantiatePrefab(item.gameObject);
                    spawnedItem.transform.SetParent(selectedFolder.transform);
                    pos = ExtensionMethods.Round(new Vector3(Building.position.x, Building.position.y + item.height / 2 + (item.height * i), Building.position.z));
                    spawnedItem.transform.position = pos;
                    lastFloorHeight += item.height;
                    //DestroyImmediate(spawnedItem.GetComponent<Dimensions>());
                }
            }

            if (item is Roof)
            {
                pos = ExtensionMethods.Round(new Vector3(Building.position.x, Building.position.y + lastFloorHeight, Building.position.z));
                pos.y += item.height;
                spawnedItem.transform.position = pos;
                //DestroyImmediate(spawnedItem.GetComponent<Dimensions>());
            }

            if (item is Door)
            {
                pos = ExtensionMethods.Round(new Vector3(Building.position.x, Building.position.y + floors[0].height / 2, Building.position.z + floors[0].length / 2));
                spawnedItem.transform.position = pos;
            }

            if (item is Window)
            {
                pos = ExtensionMethods.Round(new Vector3(Building.position.x, Building.position.y + floors[0].height / 2, Building.position.z - floors[0].length / 2));
                spawnedItem.transform.position = pos;

                int windowCount = (floorCount - 1) * 4;

                for (int i = 1; i < floorCount; i++)
                {
                    for (int j = 0; j < windowCount; j++)
                    {
                        int r = UnityEngine.Random.Range(0, 10);
                        if (r > 3)
                        {
                            spawnedItem = (GameObject)PrefabUtility.InstantiatePrefab(item.gameObject);
                            spawnedItem.transform.SetParent(selectedFolder.transform);
                            int randDir = UnityEngine.Random.Range(0, 4);
                            switch (randDir)//needs to be safed "don't spawn at same space"
                            {
                                case 0:
                                    pos = ExtensionMethods.Round(new Vector3(Building.position.x, Building.position.y + floors[0].height / 2 + (floors[0].height * i), Building.position.z + floors[0].length / 2));
                                    spawnedItem.transform.position = pos;
                                    break;
                                case 1:
                                    pos = ExtensionMethods.Round(new Vector3(Building.position.x, Building.position.y + floors[0].height / 2 + (floors[0].height * i), Building.position.z - floors[0].length / 2));
                                    spawnedItem.transform.position = pos;
                                    break;
                                case 2:
                                    pos = ExtensionMethods.Round(new Vector3(Building.position.x + floors[0].width / 2, Building.position.y + floors[0].height / 2 + (floors[0].height * i), Building.position.z));
                                    spawnedItem.transform.position = pos;
                                    break;
                                case 3:
                                    pos = ExtensionMethods.Round(new Vector3(Building.position.x - floors[0].width / 2, Building.position.y + floors[0].height / 2 + (floors[0].height * i), Building.position.z));
                                    spawnedItem.transform.position = pos;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }
        Undo.RegisterCreatedObjectUndo(selectedFolder, "Spawned Building");

        //foreach (GameObject window in windows)
        //{
        //
        //}
    }


}
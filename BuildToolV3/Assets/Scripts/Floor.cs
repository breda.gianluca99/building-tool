using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : Dimensions
{
    [SerializeField] private bool _groundFloor;
    public bool GroundFloor { 
        get => _groundFloor; 
        set => _groundFloor = value; 
    }
}

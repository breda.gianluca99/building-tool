using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Dimensions : MonoBehaviour
{
    public float height = 1f;
    public float width = 1f;
    public float length = 1f;
    public bool followScale = false;
    private void OnValidate()
    {
        if (followScale)
        {
            height  = transform.localScale.y;
            width   = transform.localScale.x;
            length  = transform.localScale.z;
        }
    }
    private void OnDrawGizmosSelected()
    {
        DrawHandles(transform.up, height / 2);
        DrawHandles(transform.right, width / 2);
        DrawHandles(transform.forward, length / 2);
    }

    private void DrawHandles(Vector3 orientation, float mesurement)
    {
        Vector3 a = transform.position;
        Vector3 b = transform.position + orientation * mesurement;

        Handles.DrawAAPolyLine(a, b);

        void DrawSphere(Vector3 p) => Gizmos.DrawSphere(p, HandleUtility.GetHandleSize(p) * 0.3f);
        DrawSphere(a);
        DrawSphere(b);
    }
}
